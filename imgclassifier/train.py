#from collections import OrderedDict
import torch
import torch.nn as nn
from torch import optim
from torch.utils.data import DataLoader
import torchvision.transforms as transforms
from torchvision.datasets import ImageFolder 
from workspace_utils import active_session, keep_awake

def train(model, learning_rate, data_dir, batch_size, epochs, dataloader_limit, print_every, device):
    train_model(model=model,
                learning_rate=learning_rate,
                train_dataloader=make_dataloader(data_dir=data_dir + '/train',
                                                 transform=make_train_transform(),
                                                 batch_size=batch_size,
                                                 shuffle=True),
                valid_dataloader=make_dataloader(data_dir=data_dir + '/valid',
                                                 transform=make_valid_transform(),
                                                 batch_size=batch_size,
                                                 shuffle=False),
                epochs=epochs,
                dataloader_limit=dataloader_limit,
                print_every=print_every,
                device=device)

normalize = transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])

def make_train_transform():
    return transforms.Compose([
                        transforms.RandomRotation(30),
                        transforms.RandomResizedCrop(224),
                        transforms.RandomHorizontalFlip(),
                        transforms.ToTensor(),
                        normalize])
    
def make_valid_transform():
    return transforms.Compose([
                        transforms.Resize(256),
                        transforms.CenterCrop(224),
                        transforms.ToTensor(),
                        normalize])
    
def make_dataloader(data_dir, transform, batch_size, shuffle):
    dataset = ImageFolder(data_dir, transform=transform)
    return DataLoader(dataset, batch_size=batch_size, shuffle=shuffle)
    
def train_model(model, learning_rate, train_dataloader, valid_dataloader, epochs, dataloader_limit, print_every, device):
    steps = 0
    model.to(device)
    criterion = nn.NLLLoss()
    optimizer = optim.Adam(model.classifier.parameters(), lr=learning_rate)
    
    for e in range(epochs):
        running_loss = 0
        for ii, (inputs, labels) in keep_awake(enumerate(train_dataloader)):
            steps += 1
            if dataloader_limit is not None and ii > dataloader_limit:
                break
            inputs, labels = inputs.to(device), labels.to(device)
            optimizer.zero_grad()
            model.train()
            outputs = model.forward(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
        
            if steps % print_every == 0:
                print("Epoch: {}/{} after steps: {}".format(e+1, epochs, steps),
                      "Training Loss {:.2f}".format(running_loss/print_every))
                running_loss = 0
                
        print("After Epoch {}/{}".format(e+1, epochs))

        print("   Training Accuracy ", end="", flush=True)
        training_loss, training_accuracy = validate(train_dataloader, model, criterion, device, dataloader_limit)
        print("{:.2f}%   Training Loss {:.2f}%".format(training_accuracy*100, training_loss*100))
        
        print("   Validation Accuracy ", end="", flush=True)
        validation_loss, validation_accuracy = validate(valid_dataloader, model, criterion, device, dataloader_limit)
        print("{:.2f}% Validation Loss {:.2f}%".format(validation_accuracy*100, validation_loss*100))
    
    print('Training finished.')

def validate(dataloader, model, criterion, device, dataloader_limit):
    correct = 0
    total = 0
    running_loss = 0
    with torch.no_grad():
        model.eval()
        for ii, (images, labels) in enumerate(dataloader):
            if dataloader_limit is not None and ii > dataloader_limit:
                break
            
            images, labels = images.to(device), labels.to(device)
            total += labels.size(0)
            outputs = model(images)

            _, predicted = torch.max(outputs.data, 1)
            correct += (predicted == labels).sum().item()
            
            running_loss += criterion(outputs, labels).item()
                        
    loss = running_loss / total
    accuracy = correct / total
    return loss, accuracy
