from collections import OrderedDict
import torch.nn as nn
import torchvision.models as models

def make_training_model(arch, hidden_units_layer_1, hidden_units_layer_2, dropout_p):
    hidden_layers = [(25088, hidden_units_layer_1, dropout_p),
                     (hidden_units_layer_1, hidden_units_layer_2, dropout_p)]    
    c = create_classifier(hidden_layers, output_size=102)
    return create_model(c, arch)
    
def create_layer(index, input_size, output_size, dropout_p):
    return [('fc{}'.format(index), nn.Linear(input_size, output_size)),
            ('relu{}'.format(index), nn.ReLU()),
            ('dropout{}'.format(index), nn.Dropout(p=dropout_p))]

def create_classifier(layers, output_size):
    c = nn.Sequential(OrderedDict(
        create_layer(1, input_size=layers[0][0], output_size=layers[0][1], dropout_p=layers[0][2]) +
        create_layer(2, input_size=layers[1][0], output_size=layers[1][1], dropout_p=layers[1][2]) +
        [('output', nn.Linear(layers[1][1], output_size)),
         ('logsoftmax', nn.LogSoftmax(dim=1))
        ]))
    return c

def create_model(classifier, arch):
    if not arch.startswith("vgg"):
        raise ValueError("only vgg models are supported")
    make_model = getattr(models, arch, None)
    if make_model is None:
        raise ValueError('no such torchvision model: ', arch)
    m = make_model(pretrained=True)
    m.arch = arch
    for p in m.parameters():
        p.requires_grad = False
    m.classifier = classifier
    return m
