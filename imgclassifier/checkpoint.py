import torch
from datetime import datetime
import os
import imgclassifier.model as ic_model


def save_checkpoint(model, save_dir):
    filename = get_checkpoint_filename()
    filepath = os.path.join(save_dir, filename)
    torch.save(get_checkpoint(model), filepath)
    return filepath

def get_checkpoint_filename():
    tstamp = datetime.now().strftime("%Y%m%d-%H%M%S")
    return "checkpoint-" + tstamp + ".pth"    

def get_checkpoint(model):
    c = model.classifier
    checkpoint = {  'classifier': { 'input_size': 25088,
                                   'output_size': 102,
                                   'hidden_layers': [
                                       (c.fc1.in_features, c.fc1.out_features, c.dropout1.p),
                                       (c.fc2.in_features, c.fc2.out_features, c.dropout2.p)],
                                   'state_dict': model.classifier.state_dict()
                                 },
                    'arch': model.arch,
#                   'class_to_idx': model.class_to_idx,
#                   'epochs_trained': 3,
#                   'optimizer_state': optimizer.state_dict
                 }
    return checkpoint

def load_model_from_checkpoint(path_to_checkpoint_file, map_location=None):        
    checkpoint = torch.load(path_to_checkpoint_file, map_location=map_location)

    c = ic_model.create_classifier(layers=checkpoint['classifier']['hidden_layers'],
                          output_size=checkpoint['classifier']['output_size'])
    c_state_dict = checkpoint['classifier']['state_dict']
    c.load_state_dict(c_state_dict)
    arch = checkpoint['arch']
    m = ic_model.create_model(c, arch)
#       m.class_to_idx = checkpoint['class_to_idx']
    return m
