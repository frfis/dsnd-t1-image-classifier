#!/usr/bin/env python3

import argparse
import os
import torch
import imgclassifier.checkpoint as ic_checkpoint
import imgclassifier.model as ic_model
import imgclassifier.train as ic_train

def main(args):
    if not os.path.isdir(args.save_dir):
        raise NotADirectoryError(args.save_dir)
    
    if args.gpu and not torch.cuda.is_available():
        raise Exception('no GPU available')
    
    if args.gpu:
        device = 'cuda:0'
    else:
        device = 'cpu'
    print("using device: {}".format(device))

    print("using pretrained torchvision model: {}".format(args.arch))

    print('training on data in dir:', args.data_dir)
    print('training for {} epochs'.format(args.epochs))
    if not args.dataloader_limit is None:
          print('NOTE: loading only the first {} items from every data loader'.format(args.dataloader_limit))

    print('learning rate:', args.learning_rate)
    print('units in 1st hidden layer:', args.hidden_units_layer_1)
    print('units in 2nd hidden layer:', args.hidden_units_layer_2)
    print('dropout rate (in every hidden layer):', args.dropout_p)

    
    model = ic_model.make_training_model(args.arch,
                                         args.hidden_units_layer_1,
                                         args.hidden_units_layer_2,
                                         args.dropout_p)
    ic_train.train(model=model,
             learning_rate=args.learning_rate,
             data_dir=args.data_dir,
             batch_size=args.batch_size,
             epochs=args.epochs,
             dataloader_limit=args.dataloader_limit,
             print_every=args.print_every,
             device=device)
    
    print("saving checkpoint...")
    checkpoint_file = ic_checkpoint.save_checkpoint(model, args.save_dir)
    print("checkpoint saved to file: ", checkpoint_file)
    print("DONE")

if __name__ == "__main__":
    argparser = argparse.ArgumentParser(
        description = 'Train a new network on a data set'
    )
    argparser.add_argument('data_dir', action='store', help='data directory')
    argparser.add_argument('--learning_rate', action='store', type=float, default=0.001, help='optimizer learning rate')
    argparser.add_argument('--hidden_units_layer_1', action='store', type=int, default=1024, help='number of units in first hidden layer')
    argparser.add_argument('--hidden_units_layer_2', action='store', type=int, default=512, help='number of units in second hidden layer')
    argparser.add_argument('--dropout_p', action='store', type=float, default=0.2, help='dropout rate used for every hidden layer')
    argparser.add_argument('--save_dir', action='store', default='.', help='directory where checkpoint will be saved')
    argparser.add_argument('--batch_size', action='store', type=int, default=32, help='batch size')
    argparser.add_argument('--epochs', action='store', type=int, default=3, help='training epochs')
    argparser.add_argument('--print_every', action='store', type=int, default=20, help='print progress every n steps')
    argparser.add_argument('--dataloader_limit', action='store', type=int, help='do not load more than that number of images from any dataloader, good for debugging')
    argparser.add_argument('--arch', action='store', default='vgg16', help='name of the pretrained torchvision model to use. NOTE only vgg models are supported, e.g. vgg16')
    argparser.add_argument('--gpu', action='store_true', default=False, help='use GPU, raises error if not available')
    
    main(argparser.parse_args())
