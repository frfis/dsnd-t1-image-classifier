#!/usr/bin/env python3

import argparse
import torch
import imgclassifier.checkpoint as ic_checkpoint
import imgclassifier.predict as ic_predict

def main(args):
    if args.gpu and not torch.cuda.is_available():
        raise Exception('no GPU available')
    
    if args.gpu:
        map_location = lambda storage, loc: storage.cuda(0)
        print("using GPU")
    else:
        map_location = lambda storage, loc: storage
        print("using CPU")

    print('loading model checkpoint from path:', args.checkpoint)
    model = ic_checkpoint.load_model_from_checkpoint(args.checkpoint, map_location)
    print('model loaded')
    print('torchvision model used: ', model.arch)
    
    print('classifying image at path:', args.input)
    probs, classes = ic_predict.predict_class(args.input, model, args.top_k)
    
    if args.category_names is None:
        name_for_class = None
    else:
        name_for_class = ic_predict.make_fn_name_for_class(args.category_names)
        
    for p, c in list(zip(probs, classes)):
        if name_for_class is None:
            print('class={} prob={:.2f}%'.format(c, p*100))
        else:
            print('name="{}" class={} prob={:.2f}%'.format(name_for_class(c), c, p*100))
     
if __name__ == "__main__":
    argparser = argparse.ArgumentParser(
        description = 'Predict flower name from an image along with the probability of that name'
    )
    argparser.add_argument('input', action='store', help='path to input image file')
    argparser.add_argument('checkpoint', action='store', help='path to model checkpoint file')
    argparser.add_argument('--category_names', action='store', help='path to json file with mapping of categories to real names')
    argparser.add_argument('--top_k', action='store', type=int, default=1, help='return top k most likely classes')
    argparser.add_argument('--gpu', action='store_true', default=False, help='use GPU, raises error if not available')

    main(argparser.parse_args())