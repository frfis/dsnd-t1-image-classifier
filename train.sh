HYPER_PARAMS='--learning_rate 0.002 --hidden_units_layer_1 512 --hidden_units_layer_2 256 --dropout_p 0.3'
PARAMS='--gpu --arch vgg19 --epochs 3 --save_dir ./checkpoints'
#PARAMS='--gpu --arch vgg19 --epochs 1 --dataloader_limit 3 --save_dir ./checkpoints'
./train.py ../aipnd-project/flowers/ $PARAMS $HYPER_PARAMS
